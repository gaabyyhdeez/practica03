package ejercicio1_codigo1COPIA;


public class HelloWorldThread extends Thread{
	
	int pid;
	String nombre;
	
	public HelloWorldThread ( String nombre ){
		
		this.nombre = nombre;
		this.pid = (int)this.getId();
	}
	
	public void run(){
		
		System.out.println( "Mi nombre es: " + this.nombre + "\nMi ID es: " + this.pid  );
	}
	
	public static void main(String[] args){
		
		HelloWorldThread t [] = new HelloWorldThread[3];
		t[0] = new HelloWorldThread("Gabriela");
		t[1] = new HelloWorldThread("Danae");
		t[2] = new HelloWorldThread("Jorge");
		
		t[0].start();
		t[1].start();
		t[2].start();
	}
}
