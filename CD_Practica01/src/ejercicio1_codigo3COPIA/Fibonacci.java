package ejercicio1_codigo3COPIA;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Fibonacci extends Thread{
	
	int n;
	int result;
	
	public Fibonacci( int n ){
		this.n = n; 
	}
	
	public void run(){
		
		if( n == 0 || n == 1 )
			result = 1;
		else {
			
			Fibonacci f1 = new Fibonacci( n-1 );
			Fibonacci f2 = new Fibonacci( n-2 );
			
			f1.start();
			f2.start();
			
			try{
				f1.join();
				f2.join();
			}
			
			catch( InterruptedException e ){};
	
			result = f1.getResult() + f2.getResult();
			
			
			///Imprimir ID y N�mero del padre y los dos hijos. 
			///System.out.println( "ID padre: " + this.getId() + " Numero: " + this.n  );
			///System.out.println( "ID hijo1: " + f1.getId() + " Numero: " + f1.getNumber()  );
			///System.out.println( "ID hijo2: " + f2.getId() + " Numero: " + f2.getNumber() + "\n");
			
			
			///Imprimir ID y Resultado del padre y los dos hijos.
			//System.out.println( "ID padre: " + this.getId() + "\nResultado: " + this.result + 
			//		" = " + f1.getResult() + "(ID:" + f1.getId() + ") + " + f2.getResult() + "(ID:" + f2.getId() + ")\n");
	}
}
	
	public int getResult(){
		return result;
	}
	
	/// Agregaci�n de funci�n "getNumber"
	public int getNumber(){
		return n;
	}
	
	
	public static void main( String[] args )  throws IOException {
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in)); 
        System.out.print("Por favor ingrese un numero: ");
        String entrada = br.readLine();
		int fib = Integer.parseInt(entrada);
		
		Fibonacci f1 = new Fibonacci( fib );
		f1.start();
		
		try {
			
			f1.join();
		}
		
		catch( InterruptedException e ){};
		System.out.println( "El resultado final es: " + f1.getResult() );
	}
}
